const User = require('../models/user')
const {
    verifyToken,
    verifyTokenAndAuthorization,
    verifyTokenAndAdmin,
} = require("../middleware/auth");
    const router = require("express").Router();



router.post('/users', async (req, res) => {
    const user = new User(req.body)
    console.log(user);
    try {
        await user.save()
        const token = user.generateAuthToken()
        res.status(201).send({ user, token })
    }
    catch(e) {
        console.log(e);
        res.status(400).send(e)
        
    }
})

router.post('/users/login', async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password)
        const token = await user.generateAuthToken()
        res.send({ user, token })
    } catch (e) {
        res.status(400).send()
    }
})

router.put('/user/:id', verifyTokenAndAuthorization, async (req,res) => {
      
  try {
      const updatedUser = await User.findByIdAndUpdate(
          req.params.id, {
              $set: req.body,
          }, {
              new: true
          }
      );
      res.status(200).send(updatedUser);
  } catch (err) {
      res.status(500).send(err);
  }

})
router.get('/user/:id', verifyTokenAndAdmin, async  (req, res) => {
    try {
        const getUser = await User.findById(req.params.id)
        res.status(200).send(getUser);
    }catch(err) {
        res.status(500).send(err);
    }
})
 
router.get('/users', verifyTokenAndAdmin, async (req, res) => {
    const query = req.query.new;
    
    try {
        const users = query ?
            await User.find().sort({
                _id: -1
            }).limit(5)
            : await User.find()
     
        res.status(200).send(users);
    } catch (err) {
        res.status(500).send(err);
    }
})


router.post('/users/logout', verifyToken, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.user.save()

        res.send()
    } catch (e) {
        res.status(500).send()
    }
})

router.post('/users/logoutAll', verifyToken, async (req, res) => {
    try {
        req.user.tokens = []
        await req.user.save()
        res.send()
    } catch (e) {
        res.status(500).send()
    }
})

router.get('/users/me', verifyToken, async (req, res) => {
    res.send(req.user)
})

router.patch('/users/me', verifyToken, async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['username', 'email', 'password', 'isAdmin']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    if (!isValidOperation) {
        return res.status(400).send({ error: 'Invalid updates!' })
    }

    try {
        updates.forEach((update) => req.user[update] = req.body[update])
        await req.user.save()
        res.send(req.user)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.delete('/users/me', verifyToken, async (req, res) => {
    try {
        await req.user.remove()
        res.send(req.user)
    } catch (e) {
        res.status(500).send()
    }
})

module.exports = router