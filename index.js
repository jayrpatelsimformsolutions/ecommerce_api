const express = require("express");
require('./db/mongoose')
const app = express();
const dotenv = require('dotenv')
dotenv.config()


// const productRouter = require('./routers/product')
// const cartRouter = require('./routers/cart')
// const orderRouter = require('./routers/order')
  app.use(express.urlencoded({extended:false}))
// app.use(cartRouter)
// app.use(productRouter)
// app.use(orderRouter)

app.use(express.json())



const userRouters = require('./routers/user')

// const cartRouters = require('./routers/cart')
 const orderRouters = require('./routers/order')
 const productRouters = require('./routers/product')
// const stripeRouters = require('./routers/stripe')
 app.use(userRouters)
// app.use(cartRouters)
app.use(orderRouters)
 app.use(productRouters)
// app.use(stripeRouters)


app.listen(process.env.PORT || 3000, () => {
    console.log("server is running");
});
